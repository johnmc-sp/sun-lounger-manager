module app.sunlounger {
	requires javafx.graphics;
	requires javafx.fxml;
	requires javafx.controls;
	requires java.desktop;
	requires javafx.base;
	requires java.logging;
	exports application.view;
	opens application;
	opens application.view;
	opens application.model;
}