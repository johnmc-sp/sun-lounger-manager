package application;

import application.model.SunBedCollection;
import application.util.View;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Main class for the Sun Lounger application.
 * The application shows a collection of sun beds that
 * are available for hire for the day. Selecting a sun 
 * bed marks it as hired, selecting it again makes it 
 * available.
 * @author 
 *
 */
public class App extends Application {

	//private static Logger logger;
	private static final Logger LOGGER = Logger.getLogger(App.class.getName());


	@Override
	public void start(Stage primaryStage) {
		LOGGER.entering(App.class.getName(), "Starting start method");

		// This just has default values for now.
		try {
			AnchorPane root = (AnchorPane) FXMLLoader
					.load(getClass().getResource("/application/view/SunBedView.fxml"));
			Scene scene = new Scene(root);

			scene.getStylesheets()
					.add(getClass().getResource("/application/resources/css/mainStyle.css").toExternalForm());
			primaryStage.getIcons()
			.add(new Image(this.getClass().getResource("/application/resources/img/sunbed.png").toExternalForm()));
			primaryStage.setTitle("Sun Lounger Manager");
			primaryStage.setScene(scene);
			primaryStage.show();

			View.centerOnScreen(primaryStage);
			LOGGER.exiting(App.class.getName(), "Ending start method");
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Before calling the JavaFX launch, ensure we can find and open
	 * the file used to hold the data.  First try opening, if that 
	 * fails, try creating and then opening the file. If that fails 
	 * then terminate the program.
	 * @param args
	 */
	public static void main(String[] args) {
		
		Handler fileHandler = null;
		Formatter simpleFormatter = null;

		try {
			// String path = System.getProperties().getProperty("user.home");
			String path = getFilePath();
			
			fileHandler = new FileHandler(path + "/sunbed.log");
			simpleFormatter = new SimpleFormatter();
			fileHandler.setFormatter(simpleFormatter);
			LOGGER.addHandler(fileHandler);
			LOGGER.setLevel(Level.ALL);
			fileHandler.setLevel(Level.ALL);
		} catch (SecurityException | IOException e2) {
			e2.printStackTrace();
		}

		LOGGER.entering(App.class.getName(), "Starting main method");
	   	// Access the file containing data for the application
			try {
				SunBedCollection.getInstance().readDataFromFile();
			} catch (IOException | ClassNotFoundException e) {
				// Try to create and then read the file
				try {
					SunBedCollection.getInstance().writeDataToFile();
					SunBedCollection.getInstance().readDataFromFile();
				} catch (IOException | ClassNotFoundException e1) {
					e1.printStackTrace();
				}
			}    	
			launch(args);
			LOGGER.exiting(App.class.getName(), "Ending main method");
	}
	

	/**
	 * Ensures the file path exists use for the log file 
	 * @return
	 * @throws IOException
	 */
	private static String getFilePath() throws IOException {
		Files.createDirectories(
				Paths.get(System.getProperty("user.home") + "/sunlounger")
				); 
		return Paths.get(System.getProperty("user.home") + "/sunlounger").toString();
	}

}
