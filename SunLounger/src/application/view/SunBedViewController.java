package application.view;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import application.App;
import application.model.SunBedCollection;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * Controller for the main Sun Lounger screen.
 * Draws the sun beds, adds and removes sun beds and 
 * clears all sun beds making them available for hire.
 * @author 
 *
 */
public class SunBedViewController implements Initializable {
	
	private static final Logger LOGGER = Logger.getLogger(App.class.getName());

	private static final int SUNBEDS_PER_ROW = 6;	// Needs to be an even number
	private static final int ROWS_PER_PAGE = 3;
	private static final int SUNBEDS_PER_PAGE = SUNBEDS_PER_ROW * ROWS_PER_PAGE;
	
	private static final int MINSUNBEDHEIGHT = 75;
	private static final int MINSUNBEDWIDTH = 50;
	private static final int MINSUNBEDPADDING = 10;
	
	//private List<Label> sunBedUIs = new ArrayList<Label>();

	@FXML
	private Label title;
	
	@FXML 
	private VBox vbox;
	
	@FXML
	private Pane pane;
	
	@FXML 
	private ImageView imageBackground;
	
	@FXML
	private TabPane tabPane;
	
	
	@Override
	/**
	 * Calls the <code>SunBedCollection</code> class to get 
	 * details about the sun beds that need to be rendered 
	 * and then draws them in the UI.
	 */
	public void initialize(URL arg0, ResourceBundle arg1) {

		LOGGER.entering(SunBedViewController.class.getName(), "Starting initialize method");

		// Find out how big the screen is
//		System.out.println(title.getText());
		imageBackground.getFitHeight();
		imageBackground.getFitWidth();
		drawSunbeds();
		LOGGER.exiting(SunBedViewController.class.getName(), "Ending initialize method");
	}
	
	@FXML
	/**
	 * Makes all sun beds unoccupied
	 */
	private void handleClearSunBeds() {
		LOGGER.entering(SunBedViewController.class.getName(), "Starting handleClearSunBeds method");
		SunBedCollection.getInstance().setAllSunBedsToFree();
		drawSunbeds();
		LOGGER.exiting(SunBedViewController.class.getName(), "Ending handleClearSunBeds method");
	}

	
	@FXML
	/**
	 * Adds a sun bed to the collection
	 */
	private void handleAddSunBed() {
		LOGGER.entering(SunBedViewController.class.getName(), "Starting handleAddSunBeds method");
		SunBedCollection.getInstance().addSunBed();
		drawSunbeds();
		LOGGER.exiting(SunBedViewController.class.getName(), "Ending handleAddSunBeds method");
	}
	
	@FXML
	/**
	 * Removes a sun bed from the collection
	 */
	private void handleRemoveSunBed() {
		LOGGER.entering(SunBedViewController.class.getName(), "Starting handleRemoveSunBeds method");
		SunBedCollection.getInstance().removeSunBed();
		drawSunbeds();
		LOGGER.exiting(SunBedViewController.class.getName(), "Ending handleRemoveSunBeds method");
	}
	
	
	private void drawSunbeds() {
		LOGGER.entering(SunBedViewController.class.getName(), "Starting drawSunbeds method");
		int sunBedCount = getSunbedCount();

		TabPane tabPane = new TabPane();
		tabPane.setPadding(new Insets(10, 10, 10, 10));
		VBox.setVgrow(tabPane, Priority.ALWAYS);
		if(vbox.getChildren().size() > 2) {
			vbox.getChildren().remove(2);
		}
		
		vbox.getChildren().add(tabPane);
		vbox.setPadding(new Insets(20, 40, 20, 40));
		
		// How many whole pages?
		int pages = sunBedCount/SUNBEDS_PER_PAGE;
	//	System.out.println("pages: "+ pages);
		for(int page = 0; page < pages; page ++) {
			tabPane.getTabs().add(buildPage(page+1, SUNBEDS_PER_PAGE));
		}

		// How many sun beds on particle page?
		int particalPage = Math.floorMod(sunBedCount, SUNBEDS_PER_PAGE);
//		System.out.println("particalPage: "+ particalPage);
		int partPage = (particalPage > 0 ? 1 : 0);
//		System.out.println("partPage: "+ partPage);
		if(partPage > 0) {
			tabPane.getTabs().add(buildPage(pages+1, particalPage));
		}
		LOGGER.exiting(SunBedViewController.class.getName(), "Ending drawSunbeds method");
	}

	
	/**
	 * Test method - provides number of sun beds
	 * @return
	 */
	private int getSunbedCount() {
		LOGGER.entering(SunBedViewController.class.getName(), "Starting getSunbedCount method");
		//return 27;
		int number = SunBedCollection.getInstance().getCount(); 
		LOGGER.exiting(SunBedViewController.class.getName(), "Ending getSunbedCount method");
		return number;
	}

	
	private Tab buildPage(int page, int sunBedsOnPage) {
		LOGGER.entering(SunBedViewController.class.getName(), "Starting buildPage method with page: " + page + " and sunBedsOnPage: " + sunBedsOnPage);

		Tab tab;
		tab = new Tab("Page " + page);
		tab.setClosable(false);
		tab.setStyle("");
		AnchorPane anchor = new AnchorPane();
		anchor.setPadding(new Insets(10, 20, 50, 20));
		tab.setContent(anchor);
		VBox vbox = buildVBox(page ,sunBedsOnPage);
		AnchorPane.setBottomAnchor(vbox, 0.0);
		AnchorPane.setTopAnchor(vbox, 0.0);
		AnchorPane.setLeftAnchor(vbox, 0.0);
		AnchorPane.setRightAnchor(vbox, 0.0);
        anchor.getChildren().add(vbox);
        LOGGER.exiting(SunBedViewController.class.getName(), "Ending buildPage method");
		return tab;
	}
	
	/**
	 * Creates a VBox used to evenly space 
	 * each row of sun beds
	 * @return
	 */
	private VBox buildVBox(int page, int sunBedsOnPage) {
		LOGGER.entering(SunBedViewController.class.getName(), "Starting buildVBox method with page: \" + page + \" and sunBedsOnPage: \" + sunBedsOnPage");
		VBox vbox = new VBox();
		vbox.setSpacing(MINSUNBEDPADDING);
		vbox = buildGridPane(vbox, page ,sunBedsOnPage);
		LOGGER.exiting(SunBedViewController.class.getName(), "Ending buildVBox method");
		return vbox;
	}
	
	/**
	 * Creates rows of grid pane for each page
	 * with columns to hold sun bed labels
	 */
	private VBox buildGridPane(VBox vbox, int page, int sunBedsOnPage) {
		LOGGER.entering(SunBedViewController.class.getName(), "Starting buildGridPane method with page: \" + page + \" and sunBedsOnPage: \" + sunBedsOnPage");
		
		int fullRows = sunBedsOnPage/SUNBEDS_PER_ROW;
//		System.out.println("Full rows: " + fullRows);
		
		int partRows = Math.floorMod(sunBedsOnPage, SUNBEDS_PER_ROW);
//		System.out.println("Part rows: " + partRows);
		
		// Build all the full rows of sun beds
		for(int items = 0; items < fullRows; items++) {
			
			GridPane gridPane = getGridPane(true);
			VBox.setVgrow(gridPane, Priority.ALWAYS);

			for(int bed = 0; bed < SUNBEDS_PER_ROW; bed++) {
				gridPane.add(
						buildSunBed((bed + 1 + (SUNBEDS_PER_ROW*items)) + ((page-1))*SUNBEDS_PER_PAGE), 
						bed, 
						0);
				ColumnConstraints column = new ColumnConstraints();
				column.setHgrow(Priority.ALWAYS);
				gridPane.getColumnConstraints().add(column);
//				System.out.print("adding bed " + (bed + 1 + (SUNBEDS_PER_ROW*items)) + " in page row: " + items);
//				System.out.println(" in column: " + bed + " row: 0");
			}
			vbox.getChildren().add(gridPane);
		}
		
		if(partRows > 0) {
//			System.out.println("Doing part rows");
			GridPane gridPane = getGridPane(true);
			VBox.setVgrow(gridPane, Priority.ALWAYS);
			if(partRows % 2 == 0 ) {
//				System.out.println("Even beds");
				// Even number of beds
				// set up each column properties as not all will have beds
				for(int col = 0; col < SUNBEDS_PER_ROW; col++) {
//					System.out.println("Building constraints: " + col);
					ColumnConstraints column = new ColumnConstraints();
					column.setHgrow(Priority.ALWAYS);
					column.setPrefWidth(MINSUNBEDWIDTH);
					gridPane.getColumnConstraints().add(column);
				}
			} else {
//				System.out.println("Odd beds");
				// Odd number of beds
				// add an extra column so we have an odd number
				// set up each column properties as not all will have beds
				ColumnConstraints column = new ColumnConstraints();
				column.setHgrow(Priority.ALWAYS);
				column.setPrefWidth(MINSUNBEDWIDTH);
				column.setPercentWidth(7.14);
				gridPane.getColumnConstraints().add(column);
				for(int col = 0; col < SUNBEDS_PER_ROW-1; col++) {
					column = new ColumnConstraints();
					column.setHgrow(Priority.ALWAYS);
					column.setPrefWidth(MINSUNBEDWIDTH);
					gridPane.getColumnConstraints().add(column);
				}
				column = new ColumnConstraints();
				column.setHgrow(Priority.ALWAYS);
				column.setPercentWidth(7.14);
				column.setPrefWidth(MINSUNBEDWIDTH);
				gridPane.getColumnConstraints().add(column);
			}

			for(int bed = 0; bed < partRows; bed++) {
				gridPane.add(
						buildSunBed(
						(bed + 1 + (SUNBEDS_PER_ROW*fullRows)) + ((page-1))*SUNBEDS_PER_PAGE),
						(SUNBEDS_PER_ROW/2)-(partRows/2) + bed, 
						0);
//				System.out.print("adding bed " + (bed + 1 + (SUNBEDS_PER_ROW*fullRows)) + " in page row: " + (fullRows+1));
//				System.out.println(" in column: " + ((SUNBEDS_PER_ROW/2)-(partRows/2) + bed) + " row: 0");			
			}
			vbox.getChildren().add(gridPane);
		}
		
		
		// Fill in empty rows
		int emptyRows = 3 - (fullRows + (partRows>0?1:0));
		
		for(int rows = 0; rows < emptyRows; rows++) {
//			System.out.println("Got empty rows: " + emptyRows);
			GridPane gridPane = getGridPane(true);
			VBox.setVgrow(gridPane, Priority.ALWAYS);
			for(int col = 0; col < SUNBEDS_PER_ROW; col++) {
//				System.out.println("Building constraints: " + col);
				ColumnConstraints column = new ColumnConstraints();
				column.setHgrow(Priority.ALWAYS);
				column.setPrefWidth(MINSUNBEDWIDTH);
				gridPane.getColumnConstraints().add(column);
			}
			vbox.getChildren().add(gridPane);
		}

		LOGGER.exiting(SunBedViewController.class.getName(), "Ending buildGridPane method");
		return vbox;
	}
	
	
	/**
	 * Creates a sun bed as a label and 
	 * add an even listener which activates
	 * when the label is clicked.
	 */
	private Label buildSunBed(int id) {
		
		LOGGER.entering(SunBedViewController.class.getName(), "Starting buildSunBed method with id: " + id);

		Color subBedAvailableColor = Color.rgb(11, 255, 167, 0.7); 

		Label label = new Label(String.valueOf(id));
		label.setAlignment(Pos.CENTER);
		label.setFont(Font.font("Verdana", FontWeight.BOLD, 33));
		label.setPrefSize(MINSUNBEDWIDTH, MINSUNBEDHEIGHT);
		label.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		if(SunBedCollection.getInstance().isOccupied(id-1)) {
			label.setBackground(new Background(
					new BackgroundFill(
							subBedAvailableColor.invert(),
							new CornerRadii(5.0), 
							new Insets(2.0))));
			
		} else {
			label.setBackground(new Background(
					new BackgroundFill(
							subBedAvailableColor,
							new CornerRadii(5.0), 
							new Insets(2.0))));
		}
		
		// Add onClick event listener
		label.setOnMouseClicked((event) -> {
			Background currentBackground = label.getBackground();
			Background newBackground = new Background(
					new BackgroundFill(
							((Color)currentBackground.getFills().get(0).getFill()).invert(), 
							new CornerRadii(5.0), 
							new Insets(2.0)));
			label.setBackground(newBackground);
			SunBedCollection.getInstance().toggleSunBed(Integer.parseInt(((Label)event.getSource()).getText())-1);
		});

		LOGGER.exiting(SunBedViewController.class.getName(), "Ending buildSunBed method");
		return label;
	}


	private GridPane getGridPane(boolean evenBeds) {
		LOGGER.entering(SunBedViewController.class.getName(), "Starting getGridPane method with evenBeds: " + evenBeds);

		GridPane gridPane = new GridPane();
		gridPane.setHgap(MINSUNBEDPADDING);
		gridPane.setGridLinesVisible(false);
		RowConstraints row = new RowConstraints();
		row.setVgrow(Priority.ALWAYS);
		row.setPrefHeight(MINSUNBEDHEIGHT);
		gridPane.getRowConstraints().add(row);
		LOGGER.exiting(SunBedViewController.class.getName(), "Ending getGridPane method");
		return gridPane;
	}
}
