package application.model;

/**
 * The <code>SunBed</code> objects are only accessed through
 * the <code>SunBedCollection</code>
 * @author 
 *
 */
public class SunBed {
    private int ID;
    private boolean booked;

    private static int nextID = 1;

    public SunBed() {
        this.ID = nextID;
        this.booked = false;
        nextID++;
    }

    public int getID() {
        return this.ID;
    }

    public void toggleBooked() {
        if (!this.booked) {
            this.booked = true;
        }

        else {
            this.booked = false;
        }
    }

/*    @Override
    public String toString() {
        if (this.booked) {
            return "Sun Bed #" + this.ID + " is booked right now.";
        }

        else {
            return "Sun Bed #" + this.ID + " is not booked right now.";
        }
    }
*/
    
    
    
    /**
     * Used to determine if the sun bed is free or occupied
     * @return
     */
    public boolean isOccupied() {
    	return booked;
    }

	@Override
	public String toString() {
		return "SunBed [ID=" + ID + ", booked=" + booked + "]";
	}

	public boolean isBooked() {
		return booked;
	}

	public void setBooked(boolean booked) {
		this.booked = booked;
	}
}
