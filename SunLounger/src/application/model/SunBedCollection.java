/**
 * 
 */
package application.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import application.App;
import application.util.XMLDataExchange;

/**
 * Manages the collection of sun beds available 
 * in the application.  There is no direct access
 * to a sun bed, all access is via this class.  All 
 * changes must be written out to file as they happen.
 * @author 
 *
 */
public class SunBedCollection {

	private static final Logger LOGGER = Logger.getLogger(SunBedCollection.class.getName());

	private static volatile SunBedCollection instance = null;
	private List<SunBed> sunBeds;
	//private static final String FILENAMEANDLOCATION = "sunloungers.ser";
	private static final String FILENAMEANDLOCATION = "/sunloungers.xml";
	private static final String PATH = System.getProperty("user.home") + "/sunlounger";

	
	/**
	 * An instance of this class can only be created 
	 * by calling the <code>getInstance()</code> method
	 * hence the constructor is private.
	 */
	private SunBedCollection() {
		super();
		LOGGER.entering(SunBedCollection.class.getName(), "Starting SunBedCollection constructor");
		sunBeds = new ArrayList<SunBed>();
		LOGGER.exiting(SunBedCollection.class.getName(), "Ending SunBedCollection constructor");
	}

	/**
	 * Returns an object of this class.  Ensures that only one
	 * instance is created.  This is an example of the singleton
	 * design pattern.
	 * @return
	 */
	public static SunBedCollection getInstance() {
		LOGGER.entering(SunBedCollection.class.getName(), "Starting getInstance method");

		if(instance == null) {
			// No object created so create one
			// Ensure no one else is accessing this class
			synchronized (SunBedCollection.class) {
				// Check there is still no object as sync can cause delay 
				if(instance == null) {
					// create a new object
					instance = new SunBedCollection();
				}
			}
		}
		LOGGER.exiting(SunBedCollection.class.getName(), "Ending getInstance method");
		// return the object
		return instance;
	}
	
	
	/**
	 * Returns the number of sun beds available
	 * @return
	 */
	public int getCount() {
		LOGGER.entering(SunBedCollection.class.getName(), "Starting getCount method");
		return sunBeds.size();
	}
	
	
	/**
	 * Adds a sun bed to the end of the collection
	 */
	public void addSunBed() {
		LOGGER.entering(SunBedCollection.class.getName(), "Starting addSunBed method");

		sunBeds.add(new SunBed());
		try {
			writeDataToFile();
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Error writing to file");
			e.printStackTrace();
		}
		LOGGER.exiting(SunBedCollection.class.getName(), "Ending addSunBed method");
	}
	
	
	/**
	 * Removes the last sun bed from the end of the collection
	 */
	public void removeSunBed() {
		LOGGER.entering(SunBedCollection.class.getName(), "Starting removeSunBed method");

		if(getCount() > 0) {
			sunBeds.remove(getCount()-1);
			try {
				writeDataToFile();
			} catch (IOException e) {
				LOGGER.log(Level.SEVERE, "Error writing to file");
				e.printStackTrace();
			}
		}
		LOGGER.exiting(SunBedCollection.class.getName(), "Ending removeSunBed method");
	}

	/**
	 * Passed the id of a sun bed, returns if the 
	 * sun bed is occupied. 
	 * @param id
	 * @return
	 */
	public boolean isOccupied(int id) {
		LOGGER.entering(SunBedCollection.class.getName(), "Starting isOccupied method with id: " + id);
		return sunBeds.get(id).isOccupied();
	}
	
	
	/**
	 * Passed the id of a sun bed, toggles its state
	 * between free and occupied
	 * @param id
	 */
	public void toggleSunBed(int id) {
		LOGGER.entering(SunBedCollection.class.getName(), "Starting toggleSunBed method with id: " + id);

		sunBeds.get(id).toggleBooked();
		try {
			writeDataToFile();
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Error writing to file");
			e.printStackTrace();
		}
		LOGGER.exiting(SunBedCollection.class.getName(), "Ending toggleSunBed method");
	}

	
	/**
	 * Make all Sun Beds available
	 */
	public void setAllSunBedsToFree() {
		LOGGER.entering(SunBedCollection.class.getName(), "Starting setAllSunBedsToFree method");

		for(SunBed bed:sunBeds) {
			if(bed.isOccupied()) {
				bed.toggleBooked();
			}
		}
		try {
			writeDataToFile();
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, "Error writing to file");
			e.printStackTrace();
		}
		LOGGER.exiting(SunBedCollection.class.getName(), "Ending setAllSunBedsToFree method");
	}
	
	
	/**
	 * Read sun bed data from file.  If there is an 
	 * error the method must throw it.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public void readDataFromFile() throws IOException, ClassNotFoundException {
		LOGGER.entering(SunBedCollection.class.getName(), "Starting readDataFromFile method");

		sunBeds = (List<SunBed>) XMLDataExchange.readData(PATH + "/" + FILENAMEANDLOCATION);

		LOGGER.exiting(SunBedCollection.class.getName(), "Ending readDataFromFile method");
	}
	
	/**
	 * Write sun bed data out to file.  If there is an
	 * error the method must throw it.
	 * @throws IOException 
	 */
	public void writeDataToFile() throws IOException {
		LOGGER.entering(SunBedCollection.class.getName(), "Starting writeDataToFile method");

		XMLDataExchange.writeData(sunBeds, PATH + "/" + FILENAMEANDLOCATION);

		LOGGER.exiting(SunBedCollection.class.getName(), "Ending writeDataToFile method");
	}

	@Override
	public String toString() {
		return "SunBedCollection [sunBeds=" + sunBeds.toString() + "]";
	}

}
