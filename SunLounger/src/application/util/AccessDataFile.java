/**
 * 
 */
package application.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import application.model.SunBed;
import application.model.SunBedCollection;


/**
 * Used to read and write sun bed data to and from
 * the file system.  If unable to read or write 
 * the file the method must throw an error.
 * 
 * @author John McNeil
 *
 */
public class AccessDataFile {

	/**
	 * Writes <code>SunBedCollection</code> data to file
	 * @throws IOException 
	 */
	public static void writeData(List<SunBed> sunBeds, String fileDestination) throws IOException {
		
		ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(fileDestination));
		oos.writeObject(sunBeds);
		oos.close();
	}
	
	/**
	 * Reads <code>SunBedCollection</code> data from file
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static List<SunBed> readData(String fileLocation) throws IOException, ClassNotFoundException {

		List<SunBed> sunBeds = null;
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileLocation));
		sunBeds = (List<SunBed>) ois.readObject();
		ois.close();

		return sunBeds;
	}
	
}
