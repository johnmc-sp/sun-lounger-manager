<h1>Sun Lounger Booking System</h1>

<h2>Description</h2>
<p>The application is to allow a sun lounger business to keep track of sun loungers available for booking during the day.</p>
<p>There are a number of sun loungers laid out on the beach that are available for rent each day.  Visitors to the beach go to the reception and book one or more loungers for the day which allows them to use the lounger for the whole day.</p>

<img src="https://softwarepulse.co.uk/wp-content/uploads/2021/10/SunLounger.jpg" width="600" height="375">

<p>The application to manage the booking of the loungers is for use by the person in reception.  At the start of the day all the loungers are available for rent.  Each lounger will be known by a number.  As visitors rent sun loungers the receptionist selects the lounger on the screen and its status moves from available to occupied.</p>

<p>To maintain the state of the application in the event of a power failure, as the available state changes the information is written out to disk.  When the application starts up, it reads in the lounger information which not only includes the availability of the lounger it also determines the number of loungers available for rent.</p>
<p>The application will have functions to allow the addition and removal of available loungers as well as a reset feature to set all loungers to available for use at the start of each day.</p>

<h2>Technologies</h2>
<p>The application is written using OpenJDK 11 with OpenJFX 11. It is distributed as a Windows MSI installer. The windows installer is created using the OpenJDK 11 JPackager.exe tool.</p>

<h2>Team Members</h2>
<p>This project is my implementation of the project brief.  Another version of the Sun Lounger brief wich can be found here:
<a href="https://github.com/Desperate-Developers/SunBedManager">Sun Bed Manager</a>
 </p>
<p>was develop by:</p>
<ul>
<li>Josh James,</li>
<li>Klaus Capani,</li>
<li>Jorid Spaha,</li>
<li>Imran Rizwan,</li>
<li>John McNeil.</li>
</ul>

<h2>Launch</h2>
<p>To see the application in action watch the short YouTube video.</p>
<ul>
<li>YouTube video [Sun Lounger Manager desktop application](https://youtu.be/6c21zVnqAp4)</li>
<li>The Windows MSI installer can be downloaded by clicking here</li>
</ul>

